use std::fs;

const LOSS: i64 = 0;
const DRAW: i64 = 3;
const WIN: i64 = 6;

#[derive(Copy, Clone)]
enum Choice {
    Invalid,
    Rock,
    Paper,
    Scissors,
}

enum HaveTo {
    LOSS,
    DRAW,
    WIN,
}


fn main() {

    let path = "src/input.txt";
    let contents = fs::read_to_string(path).expect("Could not read file");

    let result: Vec<&str> = contents.split("\n").collect();

    let mut total_score: i64 = 0;

    for line in result {
        let mut line_score: i64 = 0;
        // println!("Line: {line}");
        let game_choices: Vec<&str>= line.split(" ").collect();
        let elf_choice: Choice;
        if game_choices[0] == 'A'.to_string() {
            elf_choice = Choice::Rock;
        } else if game_choices[0] == 'B'.to_string() {
            elf_choice = Choice::Paper;
        } else if game_choices[0] == 'C'.to_string() {
            elf_choice = Choice::Scissors;
        } else {
            elf_choice = Choice::Invalid
        }

        let have_to : HaveTo;
        let have_to_string = game_choices[1];
        if have_to_string == "X" {
            have_to = HaveTo::LOSS;
        } else if have_to_string == "Y" {
            have_to = HaveTo::DRAW;
        } else if have_to_string == "Z" {
            have_to = HaveTo::WIN;
        } else {
            have_to = HaveTo::LOSS;
        }

        match have_to {
            HaveTo::LOSS => {
                line_score += LOSS;
                match elf_choice {
                    Choice::Invalid => todo!(),
                    Choice::Rock => line_score += Choice::Scissors as i64,
                    Choice::Paper => line_score += Choice::Rock as i64,
                    Choice::Scissors => line_score += Choice::Paper as i64,
                }
            },
            HaveTo::DRAW => {
                line_score += DRAW;
                line_score += elf_choice as i64;
            },
            HaveTo::WIN => {
                line_score += WIN;
                match elf_choice {
                    Choice::Invalid => todo!(),
                    Choice::Rock => line_score += Choice::Paper as i64,
                    Choice::Paper => line_score += Choice::Scissors as i64,
                    Choice::Scissors => line_score += Choice::Rock as i64,
                }
            },
        }
        // println!("Line Score: {line_score}");
        total_score += line_score;
    }

    println!("Total score: {total_score}")

}
