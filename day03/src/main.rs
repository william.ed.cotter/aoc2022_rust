use itertools::Itertools;
use std::{fs, str};

fn check_common_letter(first: &str, second: &str) -> Option<char> {
    for letter in first.chars() {
        // let converted_letter = str::from_utf8(letter).unwrap();
        if second.contains(letter) {
            return Some(letter);
        }
    }

    None
}

fn check_common_letters(first: &str, second: &str) -> Option<String> {
    // Create an empty vector for storing common letters
    let mut common_letters = Vec::new();

    // loop through letters in first array
    for letter in first.chars() {
        if second.contains(letter) {
            let mut is_in_vector: bool = false;

            for common_letter in &common_letters {
                if *common_letter == letter {
                    is_in_vector = true;
                    break;
                }
            }

            if !is_in_vector {
                common_letters.push(letter)
            }
        }
    }

    if common_letters.len() > 0 {
        return Some(common_letters.into_iter().collect());
    }

    None
}

fn check_common_letters_three(first: &str, second: &str, third: &str) -> Option<String> {
    let letters_string = check_common_letters(second, third).unwrap();
    let common_letters = check_common_letters(first, &letters_string);
    common_letters
}

fn get_priority(first: &str, letter: char) -> Option<usize> {
    for (index, first_letter) in first.char_indices() {
        if first_letter == letter {
            return Some(index + 1);
        }
    }

    None
}
fn main() {
    let filepath = "src/input.part02.txt";

    const PRIORITY: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    let mut the_sum = 0;

    let contents = fs::read_to_string(filepath).expect("SOmething went wrong loading the file");

    let num_groups = contents.lines().count() / 3;
    println!("NUM_GROUPS: {num_groups}");

    let some_contents: Vec<&str> = contents.lines().collect();

    for line_group in some_contents.chunks(3) {
        println!("{:?}", line_group);
        if line_group.len() != 3 {
            println!("Not three groups");
            continue;
        }
        let common_letters =
            check_common_letters_three(line_group[0], line_group[1], line_group[2]);
        match common_letters {
            Some(letters) => {
                let letter = letters.chars().nth(0).unwrap();
                let priority = get_priority(PRIORITY, letter).unwrap();
                the_sum += priority;
            }
            None => println!("Nothing found"),
        }
    }

    println!("Result: {the_sum}");
}
