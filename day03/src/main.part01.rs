use std::{fs, str};

fn check_common_letter(first: &str, second: &str) -> Option<char> {
    for letter in first.chars() {
        // let converted_letter = str::from_utf8(letter).unwrap();
        if second.contains(letter) {
            return Some(letter);
        }
    }

    None
}

fn get_priority(first: &str, letter: char) -> Option<usize> {
    for (index, first_letter) in first.char_indices() {
        if first_letter == letter {
            return Some(index + 1);
        }
    }

    None
}
fn main() {
    let filepath = "src/input.txt";

    const PRIORITY: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    let mut the_sum = 0;

    let contents = fs::read_to_string(filepath).expect("SOmething went wrong loading the file");
    for line in contents.lines() {
        let size = line.chars().count();
        let half = size / 2;

        let first_half = &line[..half];
        let second_half = &line[half..];

        // println!("{first_half}");
        // println!("{second_half}");

        let does_contain_index = check_common_letter(first_half, second_half);
        match does_contain_index {
            Some(x) => {
                // println!("DOES: {x}");
                let priority = get_priority(PRIORITY, x).unwrap();
                the_sum += priority;
                // println!("PRIORITY: {priority}");
            }
            None => println!("DOES NOT"),
        }
    }

    println!("The sum is {the_sum}")
}
