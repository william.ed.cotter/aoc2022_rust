use std::fs;

fn main() {
    println!("Hello, world!");

    let path = "src/calories.txt";
    let contents = fs::read_to_string(path).expect("Could not read file");
    println!("Calories:\n{contents}");
}
